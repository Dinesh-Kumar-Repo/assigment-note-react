# Note Keeper Application

This is a Note Keeper application built with React.js for the frontend and AWS Lambda for the backend. The application allows multiple users to add, view, edit and Delete notes without requiring a sign-in. 

## Features

1. **Add Note**: Users can add notes with a title, tagline, body and Pick current Date $ Time. 
The notes are displayed in a grid layout.
2. **Edit Note**: Clicking on a note opens it in a pop-up for editing.
3. **Pagination**: The application supports pagination, displaying a maximum of 6 notes per page.
4. **Pin Notes**: Clicking on opens it in a pop-up for Confarmation YES or NO.
5. **Error Handling**: Proper error handling is implemented using toast notifications.

## Technologies Used

- **Frontend**: React.js
- **Backend**: AWS Lambda, Node.js, API
- **CI/CD**: GitLab CI/CD
- **Deployment**: Firebase Hosting

## How to Run the Project

### Prerequisites

- Node.js
- AWS Account
- Firebase Account

### Live Demo

   **https://assigment-note.web.app**

### Setup

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/Dinesh-Kumar-Repo/assigment-note-react
   cd assigment-note-react

2. Install dependencies:

   **npm install**

3. Run the app locally:

   **npm start**



<p> - - - - -The End- - - - - </p>