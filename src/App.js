import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import NoteList from './components/NoteList';
import NoteForm from './components/NoteForm';
import Pagination from './components/Pagination';
import Modal from './components/Modal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import './App.css';

const API_URL = 'https://y0ftklvkxk.execute-api.ap-south-1.amazonaws.com';

const App = () => {
    const [notes, setNotes] = useState([]);
    const [pinnedNotes, setPinnedNotes] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [notesPerPage] = useState(6);
    const [editingNote, setEditingNote] = useState(null);

    useEffect(() => {
        fetchNotes();
    }, []);

    const fetchNotes = async () => {
        try {
            const response = await axios.get(`${API_URL}/notes`);
            const notesData = response.data;
            setNotes(notesData);
            setPinnedNotes(notesData.filter(note => note.pinned));
        } catch (error) {
            toast.error('Failed to fetch notes');
        }
    };

    const saveNote = async (note) => {
        try {
            if (editingNote && editingNote.noteId) {
                await axios.put(`${API_URL}/notes/${editingNote.noteId}`, note);
                toast.success('Note updated');
            } else {
                await axios.post(`${API_URL}/notes`, note);
                toast.success('Note created');
            }
            fetchNotes();
            setEditingNote(null); // Clear the editing note state after save
        } catch (error) {
            toast.error('Failed to save note');
        }
    };

    const deleteNote = async (noteId) => {
        try {
            await axios.delete(`${API_URL}/notes/${noteId}`);
            toast.success('Note deleted');
            fetchNotes();
        } catch (error) {
            toast.error('Failed to delete note');
        }
    };

    const editNote = (note) => {
        setEditingNote(note);
    };

    const pinNote = async (note) => {
        try {
            note.pinned = !note.pinned;
            await axios.put(`${API_URL}/notes/${note.noteId}`, note);
            fetchNotes();
            toast.success(note.pinned ? 'Note pinned' : 'Note unpinned');
        } catch (error) {
            toast.error('Failed to pin/unpin note');
        }
    };

    const indexOfLastNote = currentPage * notesPerPage;
    const indexOfFirstNote = indexOfLastNote - notesPerPage;
    const currentNotes = notes.slice(indexOfFirstNote, indexOfLastNote);

    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    return (
        <div className="container">
            <h1 className="my-4 text-center">Note Keeper</h1>
            <button className="btn btn-primary mb-3 btn-sm" onClick={() => setEditingNote({})}>
                Add Note <FontAwesomeIcon icon={faPlus} />
            </button>
            <NoteList notes={[...pinnedNotes, ...currentNotes]} onEdit={editNote} onDelete={deleteNote} onPin={pinNote} />
            <Pagination notesPerPage={notesPerPage} totalNotes={notes.length} paginate={paginate} />
            {editingNote && (
                <Modal onClose={() => setEditingNote(null)}>
                    <NoteForm onSave={saveNote} initialData={editingNote} onCancel={() => setEditingNote(null)} />
                </Modal>
            )}
            <ToastContainer />
        </div>
    );
};

export default App;
