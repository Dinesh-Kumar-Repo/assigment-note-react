import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave, faHeading, faTag, faAlignLeft } from '@fortawesome/free-solid-svg-icons';
import './Modal.css';

const NoteForm = ({ onSave, initialData, onCancel }) => {
    const [note, setNote] = useState(initialData || { title: '', tagline: '', body: '' });
    const [errors, setErrors] = useState({});

    const handleChange = (e) => {
        const { name, value } = e.target;
        setNote({
            ...note,
            [name]: value
        });
        setErrors({
            ...errors,
            [name]: ''
        });
    };

    const validate = () => {
        let formErrors = {};
        if (!note.title?.trim()) formErrors.title = 'Title is required';
        if (!note.tagline?.trim()) formErrors.tagline = 'Tagline is required';
        if (!note.body?.trim()) formErrors.body = 'Body is required';
        return formErrors;
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const formErrors = validate();
        if (Object.keys(formErrors).length === 0) {
            onSave(note);
        } else {
            setErrors(formErrors);
        }
    };

    return (
        <form onSubmit={handleSubmit} className="p-3 form-container">
            <div className="mb-3">
                <label htmlFor="title" className="form-label">
                    Title <FontAwesomeIcon icon={faHeading} className="ms-2" />
                </label>
                <input
                    id="title"
                    className={`form-control ${errors.title ? 'is-invalid' : ''}`}
                    name="title"
                    value={note.title}
                    onChange={handleChange}
                    placeholder="Enter title"
                />
                {errors.title && <div className="invalid-feedback">{errors.title}</div>}
            </div>
            <div className="mb-3">
                <label htmlFor="tagline" className="form-label">
                    Tagline <FontAwesomeIcon icon={faTag} className="ms-2" />
                </label>
                <input
                    id="tagline"
                    className={`form-control ${errors.tagline ? 'is-invalid' : ''}`}
                    name="tagline"
                    value={note.tagline}
                    onChange={handleChange}
                    placeholder="Enter tagline"
                />
                {errors.tagline && <div className="invalid-feedback">{errors.tagline}</div>}
            </div>
            <div className="mb-3">
                <label htmlFor="body" className="form-label">
                    Body <FontAwesomeIcon icon={faAlignLeft} className="ms-2" />
                </label>
                <textarea
                    id="body"
                    className={`form-control ${errors.body ? 'is-invalid' : ''}`}
                    name="body"
                    value={note.body}
                    onChange={handleChange}
                    placeholder="Enter body"
                    rows="2"
                />
                {errors.body && <div className="invalid-feedback">{errors.body}</div>}
            </div>
            <div className="d-flex justify-content-between">
                <button type="submit" className="btn btn-primary btn-sm">
                    <FontAwesomeIcon icon={faSave} className="me-2" /> Add +
                </button>
                <div>
                    <button type="button" className="btn btn-danger btn-sm" onClick={onCancel}>
                        Cancel
                    </button>
                </div>
            </div>
        </form>
    );
};

export default NoteForm;
