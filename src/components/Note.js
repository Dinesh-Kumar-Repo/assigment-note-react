import React, { useState } from 'react';
import './Modal.css';

const Note = ({ note, onEdit, onDelete, onPin }) => {
    const formatDate = (isoDate) => {
        const date = new Date(isoDate);
        const options = {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
        };
        const timeOptions = {
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
            hour12: true,
        };
        return `${date.toLocaleDateString('en-GB', options)}, ${date.toLocaleTimeString('en-GB', timeOptions)}`;
    };

    const [showModal, setShowModal] = useState(false);

    const handleDelete = (id) => {
        setShowModal(true);
    };

    const handleConfirmDelete = () => {
        onDelete(note.id);
        setShowModal(false);
    };

    const handleCancelDelete = () => {
        setShowModal(false);
    };

    return (
        <div className="card m-2">
            <div className="card-body">
                <h5 className="card-title">{note.title}</h5>
                <h6 className="card-subtitle">{note.tagline}</h6>
                <p className="card-text">{note.body}</p>
                <p className="card-text time">{formatDate(note.createdAt)}</p>
                <div className="btn-group d-flex justify-content-center" role="group">
                    <button className="btn btn-primary mr-2" onClick={() => onEdit(note)}>Edit</button>
                    <button className="btn btn-danger mr-2" onClick={() => handleDelete(note.id)}>Delete</button>
                    <button className="btn btn-info" onClick={() => onPin(note)}>{note.pinned ? 'Unpin' : 'Pin'}</button>
                </div>
            </div>
            {showModal && (
                <div className="modal-overlay">
                    <div className="modal d-flex justify-content-center align-items-center" tabIndex="-1" role="dialog" style={{ display: 'block', backgroundColor: 'rgba(0,0,0,0.5)' }}>
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-contents">
                                    <h5 className="modal-title">Confirmation</h5>
                                    <button type="button" className="modal-close" onClick={handleCancelDelete}>
                                        <span>&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <p className='red-color'>
                                        <strong>Are you sure, you want to delete this note?</strong>
                                    </p>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-danger" onClick={handleConfirmDelete}>Yes</button>
                                    <span> &nbsp; </span>
                                    <button type="button" className="btn btn-primary" onClick={handleCancelDelete}>No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default Note;
