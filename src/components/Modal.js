import React from 'react';
import './Modal.css';

const Modal = ({ onClose, children }) => {
    const handleClick = (e) => {
        e.stopPropagation();
    };

    return (
        <div className="modal-overlay" onClick={onClose}>
            <div className="modal-content" onClick={handleClick}>
                <button type="button" className="modal-close" onClick={onClose} aria-label="Close">
                    &times;
                </button>
                <div className="modal-body">
                    {children}
                </div>
            </div>
        </div>
    );
};

export default Modal;
