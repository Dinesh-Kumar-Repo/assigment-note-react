import React from 'react';
import Note from './Note';

const NoteList = ({ notes, onEdit, onDelete, onPin }) => {
    if (notes.length === 0) {
        return <p className="text-other"><strong>No data available. Please add some notes.</strong></p>;
    }

    return (
        <div className="d-flex flex-wrap justify-content-center">
            {notes.map(note => (
                <Note
                    key={note.id}
                    note={note}
                    onEdit={onEdit}
                    onDelete={onDelete}
                    onPin={onPin}
                />
            ))}
        </div>
    );
};

export default NoteList;
